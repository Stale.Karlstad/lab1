package rockPaperScissors;

import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

public class RockPaperScissors {
	
	public static void main(String[] args) {
    	/* 	
    	 * The code here does two things:
    	 * It first creates a new RockPaperScissors -object with the
    	 * code `new RockPaperScissors()`. Then it calls the `run()`
    	 * method on the newly created object.
         */
        new RockPaperScissors().run();
    }
    
    
    Scanner sc = new Scanner(System.in);
    int roundCounter = 1;
    int humanScore = 0;
    int computerScore = 0;
    List<String> rpsChoices = Arrays.asList("rock", "paper", "scissors");
    
    public void run() {
        while (true) {
            System.out.println("Let's play round "+roundCounter);
            roundCounter++;
            String userChoice = readInput("Your choice (Rock/Paper/Scissors)?");
            
            Random randomNumber = new Random();
            int number;
            number = randomNumber.nextInt(3);
            String computerChoice=rpsChoices.get(number);
            

            if (userChoice.equals(computerChoice)){
                System.out.println("Human chose " + userChoice+ ", computer chose " + computerChoice + ". It's a tie!");
            }            
            if (userChoice.equals("rock")){
                if (computerChoice.equals("paper")){
                    System.out.println("Human chose rock, computer chose paper. Computer wins!");
                    computerScore++;
                }
                if (computerChoice.equals("scissors")){
                    System.out.println("Human chose rock, computer chose scissors. Human wins!");
                    humanScore++;
                }
            }            
            if (userChoice.equals("paper")){
                if (computerChoice.equals("scissors")){
                    System.out.println("Human chose paper, computer chose scissors. Computer wins!");
                    computerScore++;
                }
                if (computerChoice.equals("rock")){
                    System.out.println("Human chose paper, computer chose rock. Human wins!");
                    humanScore++;
                }
            }
            if (userChoice.equals("scissors")){
                if (computerChoice.equals("rock")){
                    System.out.println("Human chose scissors, computer chose rock. Computer wins!");
                    computerScore++;
                }
                if (computerChoice.equals("paper")){
                    System.out.println("Human chose scissors, computer chose paper. Human wins!");
                    humanScore++;
                }
            }            
            System.out.println("Score: human "+ humanScore + ", computer " + computerScore);

            String proceed = readInput("Do you wish to continue playing? (y/n)?");
            if (proceed.equals("n")) {
                System.out.println("Bye bye :)");
                break;
              }           
        }
        }        
    /**
     * Reads input from console with given prompt
     * @param prompt
     * @return string input answer from user
     */
    public String readInput(String prompt) {
        System.out.println(prompt);
        String userInput = sc.next();
        return userInput;
    }
    //public void randomFromList() {
    //    List<Integer> numberList = Arrays.asList(1, 2, 3);
    //    Random rand = new Random();
     //   int randomElement = numberList.get(rand.nextInt(numberList.size()));
        
    //}
    
}
